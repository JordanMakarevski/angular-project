import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  images : string[] = ["https://imgd.aeplcdn.com/1056x594/n/cw/ec/40087/thar-exterior-right-front-three-quarter-11.jpeg?q=75&wm=1","https://st.depositphotos.com/1006706/2671/i/600/depositphotos_26715369-stock-photo-which-way-to-choose-3d.jpg", "https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aHVtYW58ZW58MHx8MHx8&w=1000&q=80", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-thsyY7pcpafX5U5CN_fkREa_Bmrvak0sRg&usqp=CAU"];
  removeImages : boolean = true;
  sizes : string[] = ["small","medium","large"];
  currentSize = this.sizes[0];
 changeSize(){
    if(this.currentSize === this.sizes[0]){
      this.currentSize = this.sizes[1];
    }else if (this.currentSize === this.sizes[1]){
      this.currentSize = this.sizes[2];
    }else{
      this.currentSize = this.sizes[0];
    }
  }
}
